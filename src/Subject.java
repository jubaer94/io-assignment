public class Subject {

    private String name, grade;
    private double marks, gradePoint;

    public Subject(String name, double marks) {
        this.name = name;
        this.marks = marks;
        // initialing the grade and grand point according to the marks
        if (this.marks >= 40 && this.marks < 50) {
            this.grade = "D";
            gradePoint = 1;
        } else if (this.marks >= 50 && this.marks < 55) {
            this.grade = "C";
            this.gradePoint = 2;
        } else if (this.marks >= 55 && this.marks < 60) {
            this.grade = "B-";
            this.gradePoint = 2.75;
        } else if (this.marks >= 60 && this.marks < 65) {
            this.grade = "B";
            this.gradePoint = 3;
        } else if (this.marks >= 65 && this.marks < 70) {
            this.grade = "A-";
            this.gradePoint = 3.50;
        } else if (this.marks >= 70 && this.marks < 80) {
            this.grade = "A";
            this.gradePoint = 4;
        } else if (this.marks >= 80) {
            this.grade = "A+";
            this.gradePoint = 5;
        } else if (this.marks < 0) {
            this.grade = "F";
            this.gradePoint = 0;
        }
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getGrade() {

        return grade;
    }


    public double getMarks() {
        return marks;
    }

    public void setMarks(double marks) {
        this.marks = marks;
    }

    public double getGradePoint() {
        return gradePoint;
    }

}
