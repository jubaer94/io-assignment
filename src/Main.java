import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        List<String> studentsInfo = new ArrayList<>();
        // give the location of the file that you want to read from
        File file = new File("/Users/jubaerbaker/Documents/Javaprojects/IO/Files/marks.txt");
        // it is the ouput destination where the genereated output files wilbe saved
        File directory = new File("/Users/jubaerbaker/Documents/Javaprojects/IO/Files/outputfiles");
         //creating an object of the file manager
        FileManager fileManager = new FileManager();
         //passing the paratemeters to the method that will creat files from the input file
        fileManager.createStudentFiles(file, directory);


    }
}