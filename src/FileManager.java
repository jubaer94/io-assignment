import com.sun.jdi.Value;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class FileManager {
    //list of student object that we will create by rading the input file
    private List<Student> studentsList = new ArrayList<>();
    // it represents each students taking each line as a students info from the input file
    List<String> studentsInfo = new ArrayList<>();


    public void createStudentFiles(File inputFileName, File DirectoryName) {
        if (inputFileName.exists()) {
            try {
                BufferedReader bufferedReader = new BufferedReader(new FileReader(inputFileName));
                String line;
                while ((line = bufferedReader.readLine()) != null) {
                    studentsInfo.add(line);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            // seperating column info from the studentinfo list for representation
            String columnInfo = studentsInfo.get(0);
            // spliting the cloumn heading from the line on the basis of coma seperator
            String[] columnTitles = columnInfo.split(",");
            //storing each of the cloumn heading as a list
            List<String> columnNames = new ArrayList<>();


            for (String words : columnTitles) {
                columnNames.add(words);
            }
//
            // opearating a for loop on the students info list where each student is saved as line and the lines represents the students info
            for (int i = 1; i < studentsInfo.size(); i++) {
                // spearating students subjects name and marks from the line on the basis of coma separatong
                String[] eachStudentInfoByWords = studentsInfo.get(i).split(",");
                // saving the student infos as a list
                List<String> eachStudentsInfo = new ArrayList<>();
                for (String word : eachStudentInfoByWords) {
                    eachStudentsInfo.add(word);
                }
                // creating a student by its name and id
                // we are retrieving the student name and id from the eachStudentsInfo list's index 0 and 1 as we know these are the name and id value
                Student student = new Student(eachStudentsInfo.get(0), eachStudentsInfo.get(1));
                for (int j = 2; j < eachStudentsInfo.size(); j++) {
                    //we are adding subjects to the student , from index 2 to and onwards the subject and grading are saved in the list
                    student.addSubject(columnNames.get(j), Integer.parseInt(eachStudentsInfo.get(j)));
                }
                studentsList.add(student);
            }
        } else {
            // if file does not exists
            System.out.println("file does not exists");
        }

        // we are checking the provided destination folder where we will save our out put files weather it is a directory or not
        if (DirectoryName.isDirectory()) {


            studentsList.forEach((student) ->
            {
                // for each student we are trying to create a file consisting the student's all infos
                try {
                    // for creating the file according to student's name-id we are doing the thing below
                    File studentFile = new File(DirectoryName + File.separator + student.getName() + "-" + student.getId() + ".txt");
                    //checking weather the file we wanting to create already exists or not, if already exists then it will not create the file
                    if (!studentFile.exists()) {
                        boolean result = studentFile.createNewFile();
                        if (result) {
                            System.out.println(student.getName() + "'s file created");
                        } else {
                            System.out.println("could not create file");
                        }
                        // getting the student's subject List that a the student consist
                         List<Subject> subjects = new ArrayList<>(student.getSubjects());
                        // creating a bufferwriter for writing the students information , here the infos will be written in file tat we created above according to students name and Id
                        BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(studentFile, true));
                        // appending the student's informations
                        String textToWrite = "\nName: " + student.getName() + " ID: " + student.getId();
                        bufferedWriter.append(textToWrite);
                        bufferedWriter.append("\n-------------------------------------------");
                        bufferedWriter.append("\nSubject |  Marks  | Grade Point  | Grade |");
                        bufferedWriter.append("\n-------------------------------------------");
                        subjects.forEach((subject) -> {
                            try {
                                bufferedWriter.append("\n" + subject.getName() + "\t\t" + subject.getMarks() + "\t\t" + subject.getGradePoint() + "\t\t\t" + subject.getGrade() + "\t |");
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        });
                        bufferedWriter.append("\n-------------------------------------------");
                        bufferedWriter.append("\n\t\t\t\tGPA =  " + student.getGPA());
                        bufferedWriter.append("\n-------------------------------------------");
                        bufferedWriter.flush();
                        bufferedWriter.newLine();

                    } else {
                        System.out.println(student.getName() + "'s the file already exists");
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }

            });
        }
    }

    public void printStudentList() {

    }


}
