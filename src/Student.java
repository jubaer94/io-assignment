import java.util.ArrayList;
import java.util.List;

public class Student {
    private List<Subject> subjects;
    private String id, name;

    public Student(String id, String name) {
        this.id = id;
        this.name = name;
        subjects = new ArrayList<>();
    }

    public List<Subject> getSubjects() {
        return subjects;
    }

    public void setSubjects(List<Subject> subjects) {
        this.subjects = subjects;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getGPA() {
        double GPA;
        double gradePointSum = 0;
        for (Subject subject : subjects) {
            gradePointSum = gradePointSum + subject.getGradePoint();
        }
        GPA = gradePointSum / subjects.size();
        return GPA;
    }

    public void addSubject(String name, double marks) {
        Subject subject = new Subject(name, marks);
        subjects.add(subject);
    }

}